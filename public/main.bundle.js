webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron text-center\">\n  <h1 class=\"display-3\">Gitlab API Explorer</h1>\n  <p class=\"lead\">This is a simple Angular app to retrieve user's projects, show its tree and calculate API links to the raw file</p>\n</div>\n\n<div class=\"container\">\n  <form>\n    <div class=\"form-group row\">\n      <label class=\"col-3 text-right col-form-label\">Gitlab username</label>\n      <input class=\"col-5 form-control\" placeholder=\"username\" autocomplete=off name=\"username\" [(ngModel)]=\"userName\">\n      <div class=\"col-3\">\n        <button class=\"btn btn-primary\" (click)=\"getUserProjects()\">Fetch Projects</button>\n      </div>\n    </div>\n  </form>\n\n  <div class=\"row\" *ngIf=\"user\">\n    <div class=\"card text-center\" style=\"width: 100%\">\n      <div class=\"card-body\">\n        <img [src]=\"user.avatar_url\" style=\"border-radius: 50%\">\n        <h4 class=\"card-title\">{{user.name}}</h4>\n        <h6 class=\"card-subtitle mb-2 text-muted\">{{user.username}}</h6>\n        <form>\n          <div class=\"form-group row\">\n            <label class=\"col-5 col-form-label text-right\">Select a Project</label>\n            <select class=\"col-7 form-control custom-select\" *ngIf=\"projects\" name=\"selectedProject\" [(ngModel)]=\"selectedProject\"\n                    (ngModelChange)=\"onSelectProject($event)\">\n              <option *ngFor=\"let p of projects\" [ngValue]=\"p\">{{p.name}}</option>\n            </select>\n          </div>\n        </form>\n        <a [href]=\"user.web_url\" class=\"card-link\">Gitlab</a>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row\" *ngIf=\"selectedProject\">\n    <div class=\"card text-center\" style=\"width: 100%\">\n      <div class=\"card-body\">\n        <img [src]=\"selectedProject.avatar_url\" *ngIf=\"selectedProject.avatar_url\" style=\"border-radius: 50%; max-height: 80px\">\n        <h4 class=\"card-title\">{{selectedProject.name}}</h4>\n        <h6 class=\"card-subtitle mb-2 text-muted\">{{selectedProject.description}}</h6>\n\n        <div class=\"row text-left\">\n          <ul>\n            <li *ngFor=\"let child of tree\">\n              <app-tree-item [treeItem]=\"child\"></app-tree-item>\n            </li>\n          </ul>\n        </div>\n\n        <a [href]=\"selectedProject.web_url\" class=\"card-link\">Gitlab</a>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row\">\n    <p class=\"small\">You can find the source code on <a href=\"https://gitlab.com/AAbdelnasser/gitlab-api-explorer\">Gitlab</a></p>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/toPromise.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_toPromise__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var treeType = 'tree';
var fileType = 'blob';
var AppComponent = (function () {
    function AppComponent(http) {
        this.http = http;
    }
    AppComponent.prototype.getUserProjects = function () {
        var _this = this;
        this.http.get("https://gitlab.com/api/v4/users?username=" + this.userName)
            .toPromise()
            .then(function (r) { return _this.user = r.json()[0]; });
        this.http.get("https://gitlab.com/api/v4/users/" + this.userName + "/projects/")
            .toPromise()
            .then(function (r) { return r.json(); })
            .then(function (projects) { return _this.projects = projects; });
    };
    AppComponent.prototype.onSelectProject = function (project) {
        var _this = this;
        var getPage = function (files, pageIndex) {
            return _this.http.get("https://gitlab.com/api/v4/projects/" + project.id + "/repository/tree?recursive=true&page=" + pageIndex + "&per_page=100")
                .toPromise()
                .then(function (r) {
                files = files.concat(r.json());
                var nextPageIndex = parseInt(r.headers.get('X-Next-Page'));
                return !nextPageIndex ? Promise.resolve(files) : getPage(files, nextPageIndex);
            });
        };
        getPage([], 1)
            .then(function (files) {
            return _this.gitlabItems = files.map(function (i) {
                if (i.type === treeType) {
                    return i;
                }
                i.link = "https://gitlab.com/api/v4/projects/" + project.id + "/repository/files/" + encodeURIComponent(i.path) + "/raw?ref=master";
                return i;
            });
        })
            .then(function () { return _this.drawTree(); });
    };
    AppComponent.prototype.drawTree = function () {
        function getParentItems(pathItems, tree) {
            return pathItems.reduce(function (items, pointName) {
                return pointName ? items.find(function (i) { return i.name === pointName; }).children : items;
            }, tree);
        }
        var sortedItems = [].concat(this.gitlabItems);
        sortedItems.sort(function (point1, point2) {
            var path1Words = point1.path.split('/');
            var path2Words = point2.path.split('/');
            return path1Words.length > path2Words.length ? 1 : -1;
        });
        var tree = [];
        sortedItems.forEach(function (i) {
            var pathItems = i.path.split('/');
            var newItem = Object.assign({}, i, { name: pathItems.pop() });
            if (i.type === treeType) {
                newItem.children = [];
            }
            getParentItems(pathItems, tree).push(newItem);
        });
        this.tree = tree;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tree_item_component__ = __webpack_require__("../../../../../src/app/tree-item.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__tree_item_component__["a" /* TreeItemComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/tree-item.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TreeItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TreeItemComponent = (function () {
    function TreeItemComponent() {
    }
    return TreeItemComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__app_component__["Item"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__app_component__["Item"]) === "function" && _a || Object)
], TreeItemComponent.prototype, "treeItem", void 0);
TreeItemComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-tree-item',
        template: "\n    <ng-container *ngIf=\"treeItem\">\n      {{treeItem.name}}\n      <ng-container *ngIf=\"treeItem.type === 'tree' && treeItem.children.length\">\n        <ul>\n          <li *ngFor=\"let child of treeItem.children\">\n            <app-tree-item [treeItem]=\"child\"></app-tree-item>\n          </li>\n        </ul>\n      </ng-container>\n      <a *ngIf=\"treeItem.type === 'blob'\" [href]=\"treeItem.link\">link</a>\n    </ng-container>\n  ",
        styles: []
    })
], TreeItemComponent);

var _a;
//# sourceMappingURL=tree-item.component.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map